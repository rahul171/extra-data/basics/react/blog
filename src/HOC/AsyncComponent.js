import React from 'react';

const asyncComponent = (fn) => {
    return class extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                component: null
            };
        }

        componentDidMount() {
            fn().then(({ default: component }) => {
                this.setState({ component });
            });
        }

        render() {
            const Component = this.state.component;

            if (!Component) {
                return <div>Loading...</div>;
            }

            return (
                <Component {...this.props} />
            );
        }
    }
}

export default asyncComponent;
