import React from 'react';
// import css from './App.module.css';

import { BrowserRouter } from 'react-router-dom';

import Blog from '../Blog/Blog';

class App extends React.Component {
    render() {
        return (
            <div className="app">
                <BrowserRouter>
                    <Blog />
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
