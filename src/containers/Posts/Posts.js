import React from 'react';
// import css from './Posts.module.css';
import postService from '../../services/post';

import PostsList from '../../components/PostsList/PostsList';

class Posts extends React.Component {
    constructor(props) {
        super(props);
        this.state = { posts: [] };
    }

    componentDidMount() {
        // don't call /posts, create a new endpoint in backend
        // which returns only that data of posts which you need
        // to display in the list. for eg. you don't have to
        // get the entire post content, but only upto a few character length.
        postService.getPosts()
            .then(posts => {
                console.log(posts);
                this.setState({ posts });
            });
    }

    handlePostItemClick = (id) => {
        // get any other detail of the post if needed using id from this.props.post
        this.props.history.push(`${this.props.match.url}/${id}`);
    }

    render() {
        return (
            <div className="posts">
                <PostsList
                    posts={this.state.posts}
                    handlePostItemClick={this.handlePostItemClick}
                />
            </div>
        );
    }
}

export default Posts;
