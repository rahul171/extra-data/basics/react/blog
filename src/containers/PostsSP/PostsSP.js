import React from 'react';
import css from './PostsSP.module.scss';
import PostsList from '../../components/PostsList/PostsList';

import postService from '../../services/post';
import PostSP from '../../components/PostSP/PostSP';

class PostsSP extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            selectedId: null
        };
    }

    componentDidMount() {
        postService.getPosts()
            .then(posts => {
                console.log(posts);

                let selectedId = null;

                if (posts.length !== 0) {
                    selectedId = posts[0]._id;
                }

                this.setState({ posts, selectedId });
            });
    }

    handlePostItemClick = (selectedId) => {
        this.setState({ selectedId });
    }

    render() {

        return (
            <div className={css.PostsSP}>
                {this.state.posts.length > 0 &&
                <PostsList
                    posts={this.state.posts}
                    handlePostItemClick={this.handlePostItemClick}
                />}
                <hr />
                {this.state.selectedId &&
                <PostSP id={this.state.selectedId} />}
            </div>
        );
    }
}

export default PostsSP;
