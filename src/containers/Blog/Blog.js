import React, {Suspense} from 'react';
// import css from './Blog.module.css';

import { Route, Switch } from 'react-router-dom';

// import Posts from '../Posts/Posts';
import NewPost from '../NewPost/NewPost';
import LandingPage from '../LandingPage/LandingPage';
import NotFound from '../NotFound/NotFound';
import Header from '../../components/Header/Header';
// import Post from '../../components/Post/Post';
import PostsSP from '../PostsSP/PostsSP';

import asyncComponent from '../../HOC/AsyncComponent';

const Posts = asyncComponent(() => {
    return import('../Posts/Posts');
});

const Post = React.lazy(() => import('../../components/Post/Post'));

class Blog extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <div className="components">
                    <Suspense fallback={<div>Loading React.lazy()</div>}>
                        <Switch>
                            <Route path="/" exact component={LandingPage} />
                            <Route path="/posts/:id" component={Post} />
                            <Route path="/posts" component={Posts} />
                            <Route path="/postssp" component={PostsSP} />
                            <Route path="/new" component={NewPost} />
                            <Route component={NotFound} />
                        </Switch>
                    </Suspense>
                </div>
            </React.Fragment>
        );
    }
}

export default Blog;
