import React from 'react';
import AddPost from '../../components/AddPost/AddPost';
// import css from './NewPost.module.css';

// this component represents a "Add new post" page.
// while "AddPost" component represents a form to add a component.
// whenever in doubt, split it out.

class NewPost extends React.Component {
    render() {
        return (
            <div className="new-post">
                NewPost
                <AddPost />
            </div>
        );
    }
}

export default NewPost;
