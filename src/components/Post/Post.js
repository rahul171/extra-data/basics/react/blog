import React from 'react';
// import css from './Post.module.css';

import { withRouter } from 'react-router-dom';
import postService from '../../services/post';

class Post extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            post: {},
            isLoading: true
        }
    }

    componentDidMount() {
        // if the component didn't unmount and you get a new prop, then you
        // need to handle that in componentDidUpdate hook. because this hook
        // won't get called at that time.
        // in componentDidUpdate, we also have to handle the infinite loop issue.
        const id = this.props.match.params.id;
        postService.getPost(id)
            .then(post => {
                console.log(post);
                this.setState({
                    post,
                    isLoading: false
                });
            });
    }

    handleDelete = () => {
        const id = this.props.match.params.id;
        postService.deletePost(id)
            .then(msg => {
                console.log(msg);
                this.props.history.push('/posts');
            });
    }

    render() {

        // might have to handle the loading logic differently if we are using componentDidUpdate()
        if (this.state.isLoading) {
            // show loader here.
            return <div>Loading...</div>
        }

        const
            title = this.state.post.title,
            content = this.state.post.content,
            author = this.state.post.author,
            tags = this.state.post.tags;

        const tagItems = this.getTagItems(tags);

        return (
            <div className="Post">
                <div>
                    <div className="title">{title}</div>
                    <div className="content">{content}</div>
                    <div className="author">{author}</div>
                    <div className="tags">{tagItems}</div>
                </div>
                <div>
                    <button onClick={this.handleDelete}>delete post</button>
                </div>
            </div>
        );
    }

    getTagItems = (tags) => {
        return tags.map((tag, index) => {
            return (
                <div
                    key={index}
                    className="tag-item"
                >{tag}</div>
            );
        });
    }
}

export default withRouter(Post);
