import React from 'react';
import css from './PostSP.module.scss';

import postService from '../../services/post';

class PostSP extends React.Component {
    constructor(props) {
        super(props);
        this.state = { post: {} };
    }

    componentDidMount() {
        this.getPost();
    }

    getPost = () => {
        postService.getPost(this.props.id)
            .then(post => {
                this.setState({ post });
            });
    }

    isSamePost = () => this.state.post._id === this.props.id;

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (!this.isSamePost()) {
            this.getPost();
        }
    }

    render() {
        if (!this.isSamePost()) {
            return <div>Loading...</div>;
        }

        const tagItems = this.getTagItems(this.state.post.tags);

        return (
            <div className={css.PostSP}>
                <div>{this.state.post.title}</div>
                <div>{this.state.post.content}</div>
                <div>{this.state.post.author}</div>
                <div>{tagItems}</div>
            </div>
        );
    }

    getTagItems = (tags) => {
        return tags.map((tag, index) => {
            return (
                <div
                    key={index}
                    className="tag-item"
                >{tag}</div>
            );
        });
    }
}

export default PostSP;
