import React from 'react';
// import css from './Label.module.scss';

const Label = (props) => {
    return (
        <label>
            <span>{props.text}</span>
            {props.children}
        </label>
    );
}

export default Label;
