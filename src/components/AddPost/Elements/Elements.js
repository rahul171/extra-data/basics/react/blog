import InputText from './InputText/InputText';
import TextArea from './TextArea/TextArea';
import Button from './Button/Button';

export { InputText, TextArea, Button };
