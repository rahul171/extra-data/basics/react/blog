import React from 'react';
// import css from './InputText.module.scss';

import Label from '../Label/Label';

class InputText extends React.Component {
    onChange = (e) => {
        this.props.onChange(this.props.name, e.target.value);
    }

    render() {
        return (
            <Label text={this.props.label}>
                <input
                    name={this.props.name}
                    type="text"
                    value={this.props.value}
                    onChange={this.onChange}
                />
            </Label>
        );
    }
}

export default InputText;
