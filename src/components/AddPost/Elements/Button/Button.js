import React from 'react';
import Label from '../Label/Label';
// import css from './Button.module.scss';

const Button = (props) => {
    const type = props.type || 'button';

    return (
        <Label text={props.label}>
            <button
                type={type}
                onClick={props.onClick}
            >{props.value}</button>
        </Label>
    );
}

export default Button;
