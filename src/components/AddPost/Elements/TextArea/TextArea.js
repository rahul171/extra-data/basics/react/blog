import React from 'react';
import Label from '../Label/Label';
// import css from './TextArea.module.scss';

class TextArea extends React.Component {
    onChange = (e) => {
        this.props.onChange(this.props.name, e.target.value);
    }

    render() {
        return (
            <Label text={this.props.label}>
            <textarea
                name={this.props.name}
                value={this.props.value}
                onChange={this.onChange}
            />
            </Label>
        );
    }
}

export default TextArea;
