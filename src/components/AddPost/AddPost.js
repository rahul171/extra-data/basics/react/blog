import React from 'react';
// import css from './AddPost.module.scss';

import ErrorMessage from './ErrorMessage/ErrorMessage';
import { InputText, TextArea, Button } from './Elements/Elements';

import postService from '../../services/post';

class AddPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.getInitialFormValues()
    }

    getInitialFormValues = () => {
        return {
            data: {
                title: '',
                content: '',
                author: '',
                tags: ''
            },
            error: ''
        };
    }

    logState = () => {
        console.log(this.state);
    }

    handleOnSubmit = (e) => {
        e.preventDefault();
        this.logState();

        if (!this.validateForm()) {
            return this.setState({ error: 'Invalid Form' });
        }

        const data = this.formatData();
        this.sendAddRequest(data);
    }

    sendAddRequest = (post) => {
        postService.createPost(post)
            .then(msg => {
                console.log(msg);
            });
    }

    validateForm = () => {
        const data = this.state.data;
        return data.title && data.content && data.author && data.tags;
    }

    formatData = () => {
        const data = {...this.state.data};
        data.tags = data.tags.split(',').map(tag => tag.trim());
        return data;
    }

    handleOnChange = (name, value) => {
        // console.log(e.target.value);
        // e goes away before accessing it in the setState when using a callback in setState.
        // have to tell react to persist it to access it there. but this might cause performance issues.
        // e.persist();
        // this.setState(state => {
        //     console.log(state.data);
        //     console.log(e.target.name);
        //     // state.data[e.target.name] = e.target.value;
        //     return state;
        // });
        const data = {...this.state.data};
        data[name] = value;
        this.setState({ data });
    }

    handleResetForm = () => {
        console.log('reset');
        this.setState(this.getInitialFormValues());
    }

    render() {
        return (
            <div className="add-post">
                <form onSubmit={this.handleOnSubmit}>
                    <div>
                        <InputText name="title"
                            label="Title"
                            value={this.state.data.title}
                            onChange={this.handleOnChange}
                        />
                    </div>
                    <div>
                        <TextArea name="content"
                            label="Content"
                            value={this.state.data.content}
                            onChange={this.handleOnChange}
                        />
                    </div>
                    <div>
                        <InputText name="author"
                            label="Author"
                            value={this.state.data.author}
                            onChange={this.handleOnChange}
                        />
                    </div>
                    <div>
                        <InputText name="tags"
                            label="Tags (Comma Separated)"
                            value={this.state.data.tags}
                            onChange={this.handleOnChange}
                        />
                    </div>
                    {this.state.error &&
                    <ErrorMessage message={this.state.error} />}
                    <div>
                        <div>
                            <Button value="Reset"
                                onClick={this.handleResetForm}
                            />
                        </div>
                        <div>
                            <Button value="Log State"
                                onClick={this.logState}
                            />
                        </div>
                        <div>
                            <Button type="submit" value="Submit" />
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default AddPost;
