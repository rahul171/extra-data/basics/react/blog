import React from 'react';
import css from './ErrorMessage.module.scss';

const ErrorMessage = (props) => {
    return (
        <div className={css.ErrorMessage}>
            <div>{props.message}</div>
        </div>
    );
};

export default ErrorMessage;
