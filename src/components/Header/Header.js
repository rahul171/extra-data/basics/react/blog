import React from 'react';
import {NavLink} from 'react-router-dom';
import css from './Header.module.css';

class Header extends React.Component {
    render() {
        return (
            <div className={css.Header}>
                <NavLink to="/" activeClassName={css.Active} exact>
                    <div className={css.NavItem}>Home</div>
                </NavLink>
                <NavLink to="/posts" activeClassName={css.Active}>
                    <div className={css.NavItem}>Posts</div>
                </NavLink>
                <NavLink to="/postssp" activeClassName={css.Active}>
                    <div className={css.NavItem}>Posts SP</div>
                </NavLink>
                <NavLink to="/new" activeClassName={css.Active}>
                    <div className={css.NavItem}>New Post</div>
                </NavLink>
            </div>
        );
    }
}

export default Header;
