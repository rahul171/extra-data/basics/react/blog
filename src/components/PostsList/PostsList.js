import React from 'react';
import PostsListItem from '../PostsListItem/PostsListItem';
// import css from './PostsList.module.css';

class PostsList extends React.Component {
    render() {
        const postItems = this.getPostItems(this.props.posts);

        return (
            <div className="PostsList">
                {postItems}
            </div>
        );
    }

    getPostItems = (posts) => {
        return posts.map(post => {
            return (
                <PostsListItem
                    key={post._id}
                    title={post.title}
                    content={post.content}
                    tags={post.tags}
                    author={post.author}
                    onClick={() => this.handlePostItemClick(post._id) }
                />
            );
        });
    }

    handlePostItemClick = (id) => {
        this.props.handlePostItemClick(id);
    }
}

export default PostsList;
