import React from 'react';
import css from './PostsListItem.module.css';

class PostsListItem extends React.Component {

    render() {
        const
            title = this.props.title,
            content = this.props.content,
            author = this.props.author,
            tags = this.props.tags;

        const tagItems = this.getTagItems(tags);

        return (
            <div
                className={css.PostListItem}
                onClick={this.props.onClick}
            >
                <div className="title">{title}</div>
                <div className="content">{content}</div>
                <div className="author">{author}</div>
                <div className="tags">{tagItems}</div>
            </div>
        );
    }

    getTagItems = (tags) => {
        return tags.map((tag, index) => {
            return (
                <div
                    key={index}
                    className="tag-item"
                >{tag}</div>
            );
        });
    }
}

export default PostsListItem;
