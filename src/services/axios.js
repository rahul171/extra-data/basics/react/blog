import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://localhost:4567'
});

instance.interceptors.response.use(response => {
    return response.data;
}, error => {
    console.log('error =>', error.response.data.error);
    return Promise.reject(error);
});

export default instance;
