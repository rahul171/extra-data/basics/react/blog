import axios from './axios';

const basePath = '/posts';

export const getPosts = () => {
    return axios.get(basePath);
}

export const getPost = (id) => {
    return axios.get(`${basePath}/${id}`);
}

export const deletePost = (id) => {
    return axios.delete(`${basePath}/${id}`);
}

export const createPost = (post) => {
    return axios.post(basePath, post);
}

export const updatePost = (id, post) => {
    return axios.patch(basePath, post);
}

export default { getPost, getPosts, updatePost, deletePost, createPost };
